package REST;

import core.admin.Admin;
import core.admin.routes.*;
import core.home.HomeGetRoute;
import core.login.LoginGetRoute;
import core.login.LoginPostRoute;
import core.logout.LogoutGetRoute;
import core.password.routes.*;
import core.signup.*;
import core.user.User;
import core.user.routes.*;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import spark.Filter;
import spark.ModelAndView;
import spark.Request;
import spark.servlet.SparkApplication;
import spark.template.handlebars.HandlebarsTemplateEngine;
import util.EmailUtil;
import util.Setup;

import java.io.File;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;

import static spark.Spark.*;
import static util.Messages.HTML.FLASH_MESSAGE_KEY;


public class App implements SparkApplication {
    private static SessionFactory factory = initSessionFactory();

    public static EmailUtil emailUtil = new EmailUtil();

    private static final String[] protectedRoutes = {"/user/", "/admin/"};

    public App() {

    }

    @Override
    public void init() {

        // port where application is running
        port(8080);

        // Sets the folder in classpath serving static files.
        staticFileLocation("/public");

        // the template engine
        HandlebarsTemplateEngine hbs = new HandlebarsTemplateEngine();

        // this way we cannot access pages without being authenticated first
        setupProtectedFilters();

        // if the user is not instance of User object then we cannot access user's pages (admin cannot access user's pages)
        before("/user/*", (request, response) -> {
            Object user = request.session(true).attribute("user");
            if (!(user instanceof User)) {
                halt(401, new HandlebarsTemplateEngine().render(new ModelAndView(null, "forbidden.hbs")));
            }
        });

        // if the user is not instance of Admin object then we cannot access admin's pages (user cannot access admin's pages)
        before("/admin/*", (request, response) -> {
            Object user = request.session(true).attribute("user");
            if (!(user instanceof Admin)) {
                halt(401, new HandlebarsTemplateEngine().render(new ModelAndView(null, "forbidden.hbs")));
            }
        });

        // controlling cache for every request
        before("/*", ((request, response) -> {
            response.header("Cache-control", "no-store, must-revalidate, private,no-cache");
            response.header("Pragma", "no-cache");
            response.header("Expires", "0");
        }));

        // map different routes for get/post requests

        get("/", new HomeGetRoute(), hbs);

        get("/sign-up/", new SignUpGetRoute(), hbs);
        post("/sign-up/", new SignUpPostRoute(), hbs);

        get("/successfullySigned/", new SuccessfullySignedGetRoute(), hbs);
        get("/sign-up/terms/", new SignUpGetTerms(), hbs);

        get("/login/", new LoginGetRoute(), hbs);
        post("/login/", new LoginPostRoute(), hbs);

        get("/logout/", new LogoutGetRoute(), hbs);

        get("/user/", new UserGetRoute(), hbs);
        post("/user/", new UserPostRoute(), hbs);

        get("/user/edit/", new UserEditGetRoute(), hbs);
        post("/user/edit/", new UserEditPostRoute(), hbs);

        get("/user/history/:username", new UserHistoryGetRoute(), hbs);
        post("/user/history/:username", new UserHistoryPostRoute(), hbs);

        get("/user/schedule/", new UserScheduleGetRoute(), hbs);

        get("/admin/", new AdminGetRoute(), hbs);
        post("/admin/", new AdminPostRoute(), hbs);

        get("/admin/edit/", new AdminEditGetRoute(), hbs);
        post("/admin/edit/", new AdminEditPostRoute(), hbs);

        get("/admin/files/*", (((request, response) -> {
            String uri = request.uri();
            String filePath = uri.substring(uri.indexOf("/admin/files/") + 12);
            filePath = URLDecoder.decode(filePath);
            File file = new File(filePath);
            if (!file.exists()) {
                response.redirect("/admin/");
            }
            // encoding cyrillic text otherwise is not recognized
            String encodedFileName = URLEncoder.encode(file.getName(), StandardCharsets.UTF_8);
            response.header("Content-disposition", "attachment; filename=" + encodedFileName + ";");
            OutputStream outputStream = response.raw().getOutputStream();
            outputStream.write(Files.readAllBytes(file.toPath()));
            outputStream.flush();
            return response.raw();
        })));

        get("/admin/reports/", new AdminReportsGetRoute(), hbs);

        get("/admin/schedules/create/", new AdminCreateSchedulesGetRoute(), hbs);
        post("/admin/schedules/create/", new AdminCreateSchedulesPostRoute(), hbs);

        get("/admin/schedules/view/", new AdminViewSchedulesGetRoute(), hbs);
        post("/admin/schedules/view/", new AdminViewSchedulesPostRoute(), hbs);

        get("/forgot/", new ForgotPasswordGetRoute(), hbs);
        post("/forgot/", new ForgotPasswordPostRoute(), hbs);

        get("/token/*", new TokenCheckGetRoute(), hbs);

        get("/expiredToken/", new ExpiredTokenGetRoute(), hbs);

        get("/changePassword/", new ChangePasswordGetRoute(), hbs);
        post("/changePassword/", new ChangePasswordPostRoute(), hbs);
    }

    @Override
    public void destroy() {

    }

    private static void setupProtectedFilters() {
        Filter f = ((request, response) -> {

            if (request.session(true).attribute("user") == null) {
                halt(401, new HandlebarsTemplateEngine().render(new ModelAndView(null, "forbidden.hbs")));
            }
        });

        for (String route : protectedRoutes) {
            before(route, f);
        }
    }

    // sets validation messages
    public static void setFlashMessage(Request req, String message) {
        req.session().attribute(FLASH_MESSAGE_KEY, message);
    }

    // retrieves validation messages
    public static String getFlashMessage(Request req) {
        String message = req.session().attribute(FLASH_MESSAGE_KEY);
        req.session().removeAttribute(FLASH_MESSAGE_KEY);
        return message;
    }

    // single instance of session factory (database connection) for entire application
    public static SessionFactory getFactory() {
        return factory;
    }

    private static SessionFactory initSessionFactory() {
        // Prepare the Hibernate configuration
        StandardServiceRegistry reg = new StandardServiceRegistryBuilder().configure().build();
        MetadataSources metaDataSrc = new MetadataSources(reg);

        // Get database connection
        Connection con = null;
        try {
            con = metaDataSrc.getServiceRegistry().getService(ConnectionProvider.class).getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        JdbcConnection jdbcCon = new JdbcConnection(con);

        // Initialize Liquibase and run the update
        Database database = null;
        try {
            database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcCon);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

        // Create Hibernate SessionFactory
        SessionFactory factory = metaDataSrc.getMetadataBuilder().build().getSessionFactoryBuilder().build();

        // populate database with initial admins and teams specified in sql files
        Liquibase liquibase = new Liquibase(Setup.DbSetup.SQL_FILE_2, new ClassLoaderResourceAccessor(), database);
        try {
            liquibase.update("admin");
        } catch (LiquibaseException e) {
            e.printStackTrace();
        }

        liquibase = new Liquibase(Setup.DbSetup.SQL_FILE_1, new ClassLoaderResourceAccessor(), database);

        try {
            liquibase.update("team");
        } catch (LiquibaseException e) {
            e.printStackTrace();
        }

        return factory;
    }

}
