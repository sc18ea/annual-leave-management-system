package dto;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

public class ReportVacationDaysDTO {

    private long totalVacationDaysRemaining;

    private long totalVacationDaysUsed;

    private String adminUsername;

    private String currentDate;

    private String teamName;

    private List<String> users;

    private long totalNumStaff;

    public ReportVacationDaysDTO(long daysAvailable, long daysConsumed, String adminUsername, String teamName, List<String> users,
                                 long totalNumStaff) {
        this.totalVacationDaysRemaining = daysAvailable;
        this.totalVacationDaysUsed = daysConsumed;
        this.adminUsername = adminUsername;
        this.teamName = teamName;
        this.users = users;
        this.currentDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        this.totalNumStaff = totalNumStaff;
    }

    public long getTotalVacationDaysRemaining() {
        return totalVacationDaysRemaining;
    }

    public void setTotalVacationDaysRemaining(long totalVacationDaysRemaining) {
        this.totalVacationDaysRemaining = totalVacationDaysRemaining;
    }

    public long getTotalVacationDaysUsed() {
        return totalVacationDaysUsed;
    }

    public void setTotalVacationDaysUsed(long totalVacationDaysUsed) {
        this.totalVacationDaysUsed = totalVacationDaysUsed;
    }

    public String getAdminUsername() {
        return adminUsername;
    }

    public void setAdminUsername(String adminUsername) {
        this.adminUsername = adminUsername;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public List<String> getUsers() {
        return Collections.unmodifiableList(users);
    }

    public long getTotalNumStaff() {
        return totalNumStaff;
    }

    public void setTotalNumStaff(long totalNumStaff) {
        this.totalNumStaff = totalNumStaff;
    }
}
