package enumerate;

public enum Type {
    PAID,
    UNPAID,
    MATERNITY,
    PATERNITY,
    SICK_LEAVE
}
