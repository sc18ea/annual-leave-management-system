package util;

public class Setup {
    // working email for administrator
    // TODO multiples emails for every administrator
    public static class Email {
        public static final String email = "admintest@mail.bg";
        public static final String password = "12345";
        public static final String host = "smtp.mail.bg";
        public static final String port = "465";
    }

    public static class DbSetup {
        public static final String SQL_FILE_1 = "team.sql";
        public static final String SQL_FILE_2 = "admin.sql";
    }
}
