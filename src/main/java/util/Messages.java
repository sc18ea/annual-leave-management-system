package util;

public class Messages {
    public static class Email {
        public static final String EMAIL_SENDING = "Email has been sent to the user!";
        public static final String OVERBOOKED = "Over 40% of the users associated with your team are on a leave during that period. ";
        public static final String OVERBOOKED_FLASH = OVERBOOKED + "Request has been automatically rejected. " + EMAIL_SENDING;
        public static final String VACATION_REQUEST = "Request for vacation has arrived in you inbox";
        public static final String SUBJECT = "Vacation request";
        public static final String RESET_PASSWORD_SUBJECT = "Reset password";
        public static final String RESET_PASSWORD_MESSAGE = "Password successfully reset!";
        public static final String TOKEN = "http://localhost:8080/token/";
        public static final String STATUS = "Your vacation request has been ";
        public static final String REASON = "Reason : ";
        public static final String RESTRICT_ANSWER_BACK_MESSAGE = "Hello, DO NOT respond to this email! ";
        public static final String PASSWORD_INSTRUCTIONS = "Follow this link to reset your password. This link will be valid for 5 minutes. ";
    }

    public static class Halt {
        public static final String USER_RESTRICTED = "Must be logged user to see the content";
        public static final String ADMIN_RESTRICTED = "Must be logged admin to see the content";
        public static final String FORBIDDEN = "Access denied!";
    }

    public static class Credentials {
        public static final String CONFIRM_PASSWORD_ERROR = "Passwords are not matching!";
        public static final String SAME_CREDENTIALS_ERROR = "The username is already taken!";
        public static final String WRONG_CREDENTIALS = "Wrong username or password!";
        public static final String PASSWORD_DOES_NOT_MATCH_REQUIREMENTS = "Password does not match the requirements!";
        public static final String RESET_PASSWORD_EMAIL = "Password reset link has been sent to your email account!";
        public static final String RESET_PASSWORD_EMAIL_ALREADY = "Password reset link has already been sent to your email account!";
        public static final String PASSWORD_SUCCESSFULLY_CHANGED = "Password successfully changed!";
        public static final String PASSWORD_DOES_NOT_MEET_CRITERIA = "New password does not meet requirements!";
    }

    public static class UserPage {
        public static final String ACCEPTED_REQUEST = "Admin will review you request";
        public static final String INVALID_REQUEST = "Invalid request!";
        public static final String SPECIFY_TYPE = "Please specify type!";
        public static final String CANCELED = "Successfully canceled!";
        public static final String UNABLE_TO_CANCEL = "Request reviewed by the admin cannot be removed!";
        public static final String PASSWORD_MISMATCH = "Passwords mismatch!";
        public static final String CURRENT_PASSWORD_MISMATCH = "Current password is not correct!";
        public static final String EMAIL_CHANGED = "Email successfully changed!";
        public static final String PASSWORD_SUCCESSFULLY_CHANGED = "Password successfully changed!";
    }

    public static class AdminPage {
        public static final String NO_REQUESTS = "There aren't any registered users on this team!";
        public static final String NO_VACATION = "No such vacation";
        public static final String SHIFT_CANCELED = "Successfully canceled shift!";
        public static final String SCHEDULE_UPDATE = "Schedule successfully updated!";
        public static final String ALREADY_WORKING = "Invalid request! The user has already been assigned.";
    }

    public static class HTML {
        public static final String NEW_LINE = "<br>";
        public static final String FLASH_MESSAGE_KEY = "flash_message";
    }

}
