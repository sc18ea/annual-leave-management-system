package util;

public class ValidationUtil {

    // validating username with regex to be between 4 and 29 symbols
    public static boolean validateUsername(String username) {
        String regularExpression = "^[[A-Z]|[a-z]][[A-Z]|[a-z]|\\d|[_]]{4,29}$";
        return username.matches(regularExpression);
    }

    // validating password with regex to be at least 8 symbols
    public static boolean validatePassword(String password) {
        String regularExpression = "^(?=\\S+$).{8,}$";
        return password.matches(regularExpression);
    }
}
