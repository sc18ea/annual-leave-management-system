package core.vacation;

import enumerate.Status;
import core.user.User;
import enumerate.Type;

import javax.persistence.*;
import java.time.LocalDate;
//the requirements for a leave request are defined
@Entity
@Table(name = "vacations")
public class Vacation {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "startDate")
    private LocalDate startDate;

    @Column(name = "endDate")
    private LocalDate endDate;

    @Column(name = "type")
    private Type type;

    @Column(name = "status")
    private Status status;

    @Column(name = "days")
    private int days;

    @Column(name = "filePath")
    private String filePath;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;


    public Vacation() {
    }

    public Vacation(LocalDate startDate, LocalDate endDate, Type type, Status status, User user, int days, String filePath) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
        this.status = status;
        this.user = user;
        this.days = days;
        this.filePath = filePath;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
