package core.vacation;

import core.user.User;
import dto.RequestDTO;
import enumerate.Type;

import java.util.List;

public interface VacationService {
	Vacation addVacation(User user, String from, String to, Type type, String filePath);

	Vacation cancelVacation(Vacation vacation);

	Vacation getVacationById(long vacationId);

	List<Vacation> listRequestedVacations(long userID, long teamID);

	RequestDTO ifAcceptableRequest(User user, String from, String to, Type type);

	boolean ifCancelable(Vacation vacation);
}
