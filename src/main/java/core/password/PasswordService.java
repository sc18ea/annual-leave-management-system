package core.password;

public interface PasswordService {
    void addToken(Token token);

    void removeToken(Token token);

    Token getToken(String token);

    Token getTokenByEmail(String email);
}
