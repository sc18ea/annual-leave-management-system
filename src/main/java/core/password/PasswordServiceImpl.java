package core.password;

import REST.App;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class PasswordServiceImpl implements PasswordService {

    private final SessionFactory factory;

    public PasswordServiceImpl() {
        factory = App.getFactory();
    }

    @Override
    public void addToken(Token token) {
        Transaction transaction = null;

        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            session.save(token);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    @Override
    public void removeToken(Token token) {
        Transaction transaction = null;

        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            session.delete(token);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    @Override
    public Token getToken(String token) {
        Transaction transaction = null;
        Token tok = null;

        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            String str = "from core.password.Token where token=:token";
            Query query = session.createQuery(str);
            query.setParameter("token", token);
            List list = query.list();
            if (list.size() > 0) {
                tok = (Token) list.get(0);
            }
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return tok;
    }

    @Override
    public Token getTokenByEmail(String email) {
        Transaction transaction = null;
        Token tok = null;

        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            String str = "from core.password.Token where email=:email";
            Query query = session.createQuery(str);
            query.setParameter("email", email);
            List list = query.list();
            if (list.size() > 0) {
                tok = (Token) list.get(0);
            }
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return tok;
    }
}
