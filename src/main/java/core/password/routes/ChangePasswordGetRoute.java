package core.password.routes;

import core.password.Token;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;
import java.util.Map;

import static REST.App.getFlashMessage;
import static util.Messages.HTML.FLASH_MESSAGE_KEY;

public class ChangePasswordGetRoute implements TemplateViewRoute {
    @Override
    public ModelAndView handle(Request request, Response response) {
        Map<String, String> model = new HashMap<>();
        String token = request.session(true).attribute("token");
        model.put("token", token);
        model.put(FLASH_MESSAGE_KEY, getFlashMessage(request));
        return new ModelAndView(model, "changePassword.hbs");
    }
}
