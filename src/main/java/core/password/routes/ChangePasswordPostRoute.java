package core.password.routes;

import REST.App;
import core.password.PasswordService;
import core.password.PasswordServiceImpl;
import core.password.Token;
import core.user.User;
import core.user.UserService;
import core.user.UserServiceImpl;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
import util.CipherUtil;
import util.EmailUtil;
import util.Messages;
import util.ValidationUtil;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static util.Messages.Email.RESET_PASSWORD_MESSAGE;
import static util.Messages.Email.RESET_PASSWORD_SUBJECT;

public class ChangePasswordPostRoute implements TemplateViewRoute {

    private final UserService userService;

    private final PasswordService passwordService;

    private final EmailUtil emailUtil;

    public ChangePasswordPostRoute() {
        this.emailUtil = new EmailUtil();
        this.userService = new UserServiceImpl();
        this.passwordService = new PasswordServiceImpl();
    }

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {

        String password = request.queryParams("password");
        boolean validPassword = ValidationUtil.validatePassword(password);
        String confirm_password = request.queryParams("confirm_password");
        String tokenRequest = request.queryParams("token");

        //checks if token is valid
        Token token = passwordService.getToken(tokenRequest);
        Date date = new Date();
        long diff = date.getTime() - token.getExpirationTime().getTime();
        long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
        if (minutes >= 5) {
            passwordService.removeToken(token);
            response.redirect("/expiredToken/");
            return new ModelAndView(null, "expiredToken.hbs");
        }
        //checks if both passwords are matching
        if (!validPassword) {
            App.setFlashMessage(request, Messages.Credentials.PASSWORD_DOES_NOT_MATCH_REQUIREMENTS);
            response.redirect("/changePassword/");
            return new ModelAndView(null, "changePassword.hbs");
        }

        if (!password.equals(confirm_password)) {
            App.setFlashMessage(request, Messages.Credentials.CONFIRM_PASSWORD_ERROR);
            response.redirect("/changePassword/");
            return new ModelAndView(null, "changePassword.hbs");
        }
        //sends token to the specified email
        User user = userService.getUserByEmail(token.getEmail());
        user.setPassword(CipherUtil.encrypt(password));
        userService.updateUser(user);
        passwordService.removeToken(token);
        emailUtil.sendEmailTo(user.getEmail(), RESET_PASSWORD_SUBJECT, RESET_PASSWORD_MESSAGE);
        App.setFlashMessage(request, Messages.Credentials.PASSWORD_SUCCESSFULLY_CHANGED);
        response.redirect("/login/");
        return new ModelAndView(null, "login.hbs");
    }
}
