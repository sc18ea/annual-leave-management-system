package core.password.routes;

import core.password.PasswordService;
import core.password.PasswordServiceImpl;
import core.password.Token;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TokenCheckGetRoute implements TemplateViewRoute {

    private final PasswordService passwordService;

    public TokenCheckGetRoute() {
        this.passwordService = new PasswordServiceImpl();
    }
    //checks if the provided token for change of password
    //token is set to be valid only 5 minutes
    @Override
    public ModelAndView handle(Request request, Response response) {
        String tokenRequest = request.uri().substring(7);
        Token token = passwordService.getToken(tokenRequest);
        Date date = new Date();
        long diff = date.getTime() - token.getExpirationTime().getTime();
        long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
        if (minutes >= 5) {
            passwordService.removeToken(token);
            response.redirect("/expiredToken/");
            return new ModelAndView(null, "expiredToken.hbs");
        }
        request.session(true).attribute("token", token.getToken());
        response.redirect("/changePassword/");
        return new ModelAndView(null, "changePassword.hbs");
    }
}
