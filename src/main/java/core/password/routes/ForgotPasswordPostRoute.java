package core.password.routes;

import REST.App;
import core.password.PasswordServiceImpl;
import core.password.Token;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
import util.EmailUtil;
import util.Messages;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class ForgotPasswordPostRoute implements TemplateViewRoute {

    private final EmailUtil emailUtil;

    private final PasswordServiceImpl passwordService;

    public ForgotPasswordPostRoute() {
        this.emailUtil = new EmailUtil();
        this.passwordService = new PasswordServiceImpl();
    }

    @Override
    public ModelAndView handle(Request request, Response response) {

        String email = request.queryParams("email");
        String resetToken = UUID.randomUUID().toString();
        Token token = new Token(email, resetToken, new Date());
        Token existing = passwordService.getTokenByEmail(token.getEmail());
        if (existing != null) {
            Date date = new Date();
            long diff = date.getTime() - existing.getExpirationTime().getTime();
            long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
            // token expiration time is 5 minutes
            if (minutes >= 5) {
                passwordService.removeToken(existing);
            } else {
                App.setFlashMessage(request, Messages.Credentials.RESET_PASSWORD_EMAIL_ALREADY);
                response.redirect("/forgot/");
                return new ModelAndView(null, "forgot.hbs");
            }
        }
        emailUtil.sendEmailResetToken(email, resetToken);
        passwordService.addToken(token);
        App.setFlashMessage(request, Messages.Credentials.RESET_PASSWORD_EMAIL);
        response.redirect("/forgot/");
        return new ModelAndView(null, "forgot.hbs");
    }
}
