package core.password.routes;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;
import java.util.Map;

import static REST.App.getFlashMessage;
import static util.Messages.HTML.FLASH_MESSAGE_KEY;

public class ForgotPasswordGetRoute implements TemplateViewRoute {

    @Override
    public ModelAndView handle(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        model.put(FLASH_MESSAGE_KEY, getFlashMessage(request));
        return new ModelAndView(model, "forgot.hbs");
    }
}
