package core.user.routes;

import REST.App;
import core.user.User;
import core.user.UserServiceImpl;
import core.vacation.Vacation;
import org.apache.commons.lang3.StringUtils;
import spark.*;
import util.Messages;

import java.util.*;

import static spark.Spark.halt;
import static util.Messages.HTML.NEW_LINE;
import static util.Messages.Halt.FORBIDDEN;

public class UserHistoryGetRoute implements TemplateViewRoute {

	private final UserServiceImpl userService;

	public UserHistoryGetRoute() {
		this.userService = new UserServiceImpl();
	}

	@Override
	public ModelAndView handle(Request request, Response response) {

		User user = userService.getUserByName(request.params(":username"));
		User currUser = request.session(true).attribute("user");
		if (user == null || !currUser.getUsername().equals(user.getUsername())) {
			halt(401, FORBIDDEN);
		}
		Set<Vacation> vacations = user.getVacations();

		List<Vacation> vacationsSorted = new ArrayList<>(vacations);
		vacationsSorted.sort(Comparator.comparingLong(Vacation::getId).reversed());

		//publishes request to the table in user requests
		Map<String, Object> model = new HashMap<>();
		String flashMessage = App.getFlashMessage(request);
		model.put(Messages.HTML.FLASH_MESSAGE_KEY, flashMessage);
		if (StringUtils.isNotBlank(user.getUsername())) {
			model.put("history", vacationsSorted);
			return new ModelAndView(model, "history.hbs");
		} else {
			response.status(404);
			return new ModelAndView(model, "not-found.hbs");
		}
	}
}
