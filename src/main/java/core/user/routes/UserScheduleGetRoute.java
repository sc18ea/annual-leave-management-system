package core.user.routes;

import core.admin.AdminSchedule;
import core.admin.AdminService;
import core.admin.AdminServiceImpl;
import core.user.User;
import core.user.UserService;
import core.user.UserServiceImpl;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserScheduleGetRoute implements TemplateViewRoute {

    private final AdminService adminService;

    private final UserService userService;

    public UserScheduleGetRoute() {
        this.adminService = new AdminServiceImpl();
        this.userService = new UserServiceImpl();
    }

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        Map<String, Object> model = new HashMap<>();
        User user = request.session(true).attribute("user");
        long teamId = user.getTeam().getId();
        List<User> users = userService.getUsersByTeamAndAdminId(teamId);
        List<AdminSchedule> schedules = new ArrayList<>();
        for (User u : users) {
            AdminSchedule schedule = adminService.getScheduleByUsername(u.getUsername());
            if (schedule != null) {
                schedules.add(schedule);
            }
        }

        model.put("schedules", schedules);
        return new ModelAndView(model, "userViewSchedule.hbs");
    }
}
