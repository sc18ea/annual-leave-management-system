package core.user.routes;

import REST.App;
import core.admin.Admin;
import core.admin.AdminServiceImpl;
import core.user.User;
import core.user.UserServiceImpl;
import core.vacation.Vacation;
import core.vacation.VacationServiceImpl;
import dto.RequestDTO;
import enumerate.Type;
import org.apache.commons.lang3.StringUtils;
import spark.*;
import spark.utils.IOUtils;
import util.Messages;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.io.*;

import static REST.App.emailUtil;
import static util.Messages.Email.RESTRICT_ANSWER_BACK_MESSAGE;

public class UserPostRoute implements TemplateViewRoute {

	private final AdminServiceImpl adminService;

	private final UserServiceImpl userService;

	private final VacationServiceImpl vacationService;

	private static final String FILE_DIR = System.getProperty("user.dir") + File.separator + "files" + File.separator;

	public UserPostRoute() {
		this.adminService = new AdminServiceImpl();
		this.userService = new UserServiceImpl();
		this.vacationService = new VacationServiceImpl();
	}

	@Override
	public ModelAndView handle(Request request, Response response) throws ServletException, IOException {
		request.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement(File.separator + "temp"));
		String from = request.raw().getParameter("from");
		String to = request.raw().getParameter("to");
		String typeRequest = request.raw().getParameter("type");
		Part filePart = request.raw().getPart("filename");
		User user = request.session(true).attribute("user");
		String filePath = FILE_DIR + user.getUsername() + "-" + filePart.getSubmittedFileName();

		Admin reeferAdmin = adminService.getAdminByTeamId(user.getTeam().getId());

		if (StringUtils.isEmpty(typeRequest)) {
			App.setFlashMessage(request, Messages.UserPage.SPECIFY_TYPE);
			response.redirect("/user/");
			return new ModelAndView(null, "user.hbs");
		}

		Type type = Type.valueOf(typeRequest.toUpperCase());

		RequestDTO requestDTO = vacationService.ifAcceptableRequest(user, from, to, type);
		boolean acceptable = requestDTO.isAcceptable();

		if (acceptable) {
			App.setFlashMessage(request, Messages.UserPage.ACCEPTED_REQUEST);
			Vacation vacation = vacationService.addVacation(user, requestDTO.getFrom().toString(),
					requestDTO.getTo().toString(), type, filePath);
			//true for adding | false for removing
			userService.updateVacationInfo(user, vacation, true);
			uploadFile(filePart, user.getUsername());
			StringBuilder message = new StringBuilder();
			message.append(RESTRICT_ANSWER_BACK_MESSAGE).append(System.lineSeparator())
					.append(Messages.Email.VACATION_REQUEST);
			emailUtil.sendEmailTo(reeferAdmin.getEmail(), Messages.Email.SUBJECT, message.toString());
		} else {
			App.setFlashMessage(request, Messages.UserPage.INVALID_REQUEST);
		}

		response.redirect("/user/");
		return new ModelAndView(null, "user.hbs");
	}


	private void uploadFile(Part filePart, String username) {
		String fileName = filePart.getSubmittedFileName();
		if (StringUtils.isNotEmpty(fileName)) {
			try (InputStream inputStream = filePart.getInputStream()) {
				OutputStream outputStream = new FileOutputStream(FILE_DIR + username + "-" + fileName);
				IOUtils.copy(inputStream, outputStream);
				outputStream.close();
			} catch (IOException e) {
				//TODO LOG messages?
			}
		}
	}
}
