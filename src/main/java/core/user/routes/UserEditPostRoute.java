package core.user.routes;

import REST.App;
import core.user.User;
import core.user.UserService;
import core.user.UserServiceImpl;
import org.apache.commons.lang3.StringUtils;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
import util.CipherUtil;
import util.Messages;
import util.ValidationUtil;

public class UserEditPostRoute implements TemplateViewRoute {

    private final UserService userService;

    public UserEditPostRoute() {
        userService = new UserServiceImpl();
    }
    //post route to change user password or email
    //checks if the entered passwords are matching
    //requires password to change the email
    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String email = request.queryParams("email");
        String password = request.queryParams("password");

        String currentPassword = request.queryParams("currentPassword");
        String newPassword = request.queryParams("newPassword");
        String confirmPassword = request.queryParams("confirmPassword");

        User user = request.session(true).attribute("user");

        if (StringUtils.isNotBlank(email) && StringUtils.isNotBlank(password)) {
            if (CipherUtil.decrypt(user.getPassword()).equals(password)) {
                user.setEmail(email);
                userService.updateUser(user);
                App.setFlashMessage(request, Messages.UserPage.EMAIL_CHANGED);
            } else {
                App.setFlashMessage(request, Messages.UserPage.CURRENT_PASSWORD_MISMATCH);
            }
        } else if (StringUtils.isNotBlank(currentPassword) && StringUtils.isNotBlank(newPassword) &&
                StringUtils.isNotBlank(confirmPassword)) {
            if (CipherUtil.decrypt(user.getPassword()).equals(currentPassword)) {
                if (newPassword.equals(confirmPassword)) {
                    boolean isValidPassword = ValidationUtil.validatePassword(newPassword);
                    if (!isValidPassword) {
                        App.setFlashMessage(request, Messages.Credentials.PASSWORD_DOES_NOT_MEET_CRITERIA);
                    } else {
                        user.setPassword(CipherUtil.encrypt(newPassword));
                        userService.updateUser(user);
                        App.setFlashMessage(request, Messages.UserPage.PASSWORD_SUCCESSFULLY_CHANGED);
                    }
                } else {
                    App.setFlashMessage(request, Messages.UserPage.PASSWORD_MISMATCH);
                }
            } else {
                App.setFlashMessage(request, Messages.UserPage.CURRENT_PASSWORD_MISMATCH);
            }
        }

        response.redirect("/user/edit/");
        return new ModelAndView(null, "editUser.hbs");
    }
}
