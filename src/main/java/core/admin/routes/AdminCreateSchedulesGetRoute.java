package core.admin.routes;

import REST.App;
import core.admin.Admin;
import core.team.Team;
import core.team.TeamServiceImpl;
import core.user.User;
import core.user.UserServiceImpl;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static util.Messages.AdminPage.NO_REQUESTS;
import static util.Messages.HTML.FLASH_MESSAGE_KEY;

public class AdminCreateSchedulesGetRoute implements TemplateViewRoute {

    private final TeamServiceImpl teamService;

    private final UserServiceImpl userService;

    public AdminCreateSchedulesGetRoute() {
        teamService = new TeamServiceImpl();
        userService = new UserServiceImpl();
    }
//get route for the scheduling functionality of the admin
    @Override
    public ModelAndView handle(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        Admin admin = request.session(true).attribute("user");
        Team team = teamService.getTeamByAdminId(admin.getId());
        List<User> users = userService.getUsersByTeamID(team.getId());

        if (users == null) {
            Map<String, String> noResult = new HashMap<>();
            noResult.put("nothing", NO_REQUESTS);
            noResult.put(FLASH_MESSAGE_KEY, App.getFlashMessage(request));
            return new ModelAndView(noResult, "createSchedule.hbs");
        }
        List<String> result = new ArrayList();
        users.forEach(u -> result.add(u.getUsername()));
        model.put("listUsers", result);
        model.put(FLASH_MESSAGE_KEY, App.getFlashMessage(request));
        return new ModelAndView(model, "createSchedule.hbs");
    }
}
