package core.admin.routes;

import REST.App;
import core.admin.AdminSchedule;
import core.admin.AdminService;
import core.admin.AdminServiceImpl;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
import util.Messages;

import static REST.App.setFlashMessage;


public class AdminViewSchedulesPostRoute implements TemplateViewRoute {

    private final AdminService adminService;

    public AdminViewSchedulesPostRoute() {
        this.adminService = new AdminServiceImpl();
    }
    //the function allows the admin to view an delete requests
    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {

        long requestId = Long.parseLong(request.queryParams("scheduleId"));
        AdminSchedule schedule = adminService.getScheduleById(requestId);
        if (schedule != null) {
            adminService.removeSchedule(schedule);
            setFlashMessage(request, Messages.AdminPage.SHIFT_CANCELED);
        } else {
            App.setFlashMessage(request, Messages.UserPage.INVALID_REQUEST);
        }
        response.redirect("/admin/schedules/view/");
        return new ModelAndView(null, "viewDeleteSchedules.hbs");
    }
}
