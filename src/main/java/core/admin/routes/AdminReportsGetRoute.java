package core.admin.routes;

import core.admin.Admin;
import core.admin.AdminServiceImpl;
import core.user.UserServiceImpl;
import spark.*;

import java.util.HashMap;
import java.util.Map;

public class AdminReportsGetRoute implements TemplateViewRoute {
    private final UserServiceImpl userService;

    private final AdminServiceImpl adminService;

    public AdminReportsGetRoute() {
        userService = new UserServiceImpl();
        adminService = new AdminServiceImpl();
    }
//updates user allowance statistics
    @Override
    public ModelAndView handle(Request request, Response response) {

        Map<String, String> model = new HashMap<>();

        Admin admin = request.session(true).attribute("user");

        long adminID = admin.getId();
        long daysAvailable = userService.getDaysAvailable(adminID);
        long daysConsumed = userService.getDaysConsumed(adminID);

        String vacationLink = adminService.generateVacationReport(daysAvailable, daysConsumed, admin);
        model.put("vacationLink", vacationLink);

        return new ModelAndView(model, "generated.hbs");
    }
}
