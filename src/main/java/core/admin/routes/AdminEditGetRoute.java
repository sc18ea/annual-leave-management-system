package core.admin.routes;

import REST.App;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;
import java.util.Map;

import static util.Messages.HTML.FLASH_MESSAGE_KEY;
// get route to change admin account's password
public class AdminEditGetRoute implements TemplateViewRoute {
    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        Map<String, Object> model = new HashMap<>();
        model.put(FLASH_MESSAGE_KEY, App.getFlashMessage(request));

        return new ModelAndView(model, "editAdmin.hbs");
    }
}
