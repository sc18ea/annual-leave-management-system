package core.admin.routes;

import REST.App;
import core.admin.Admin;
import core.admin.AdminSchedule;
import core.admin.AdminService;
import core.admin.AdminServiceImpl;
import core.user.User;
import core.user.UserService;
import core.user.UserServiceImpl;
import core.vacation.Vacation;
import core.vacation.VacationService;
import core.vacation.VacationServiceImpl;
import enumerate.Status;
import spark.*;

import java.util.*;
import java.util.stream.Collectors;

import static util.Messages.AdminPage.NO_REQUESTS;
import static util.Messages.HTML.FLASH_MESSAGE_KEY;

public class AdminGetRoute implements TemplateViewRoute {

    private final UserService userService;

    private final VacationService vacationService;

    private final AdminService adminService;

    public AdminGetRoute() {

        userService = new UserServiceImpl();
        vacationService = new VacationServiceImpl();
        adminService = new AdminServiceImpl();
    }
    // sends user leave requests to the admin
    @Override
    public ModelAndView handle(Request request, Response response) {

        Map<String, Object> model = new HashMap<>();

        Admin admin = request.session(true).attribute("user");
        long teamID = admin.getTeam().getId();

        model.put("adminName", admin.getUsername());
        List<User> users = userService.getUsersByTeamID(teamID);
        if (users != null && users.isEmpty()) {
            model.put("requestList", NO_REQUESTS);
            model.put(FLASH_MESSAGE_KEY, App.getFlashMessage(request));
            return new ModelAndView(model, "admin.hbs");
        }

        List<Vacation> vacations = new ArrayList<>();
        if (users != null) {
            for (User user : users) {
                vacations.addAll(vacationService.listRequestedVacations(user.getId(), teamID));
            }
        }

        List<Vacation> filtered = vacations.stream().filter(v -> v.getStatus().equals(Status.PENDING)).collect(Collectors.toList());
        model.put("requestList", filtered);
        model.put(FLASH_MESSAGE_KEY, App.getFlashMessage(request));
        return new ModelAndView(model, "admin.hbs");
    }
}

