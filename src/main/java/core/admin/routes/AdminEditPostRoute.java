package core.admin.routes;

import REST.App;
import core.admin.Admin;
import core.admin.AdminService;
import core.admin.AdminServiceImpl;
import org.apache.commons.lang3.StringUtils;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
import util.CipherUtil;
import util.Messages;
import util.ValidationUtil;

public class AdminEditPostRoute implements TemplateViewRoute {

    private final AdminService adminService;


    public AdminEditPostRoute() {
        this.adminService = new AdminServiceImpl();
    }
    // get route to change admin account's password
    // checks if the inputted passwords match
    //updates the new password in the database
    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String currentPassword = request.queryParams("currentPassword");
        String newPassword = request.queryParams("newPassword");
        String confirmPassword = request.queryParams("confirmPassword");

        Admin admin = request.session(true).attribute("user");

        if (StringUtils.isNotBlank(currentPassword) && StringUtils.isNotBlank(newPassword) &&
                StringUtils.isNotBlank(confirmPassword)) {
            if (CipherUtil.decrypt(admin.getPassword()).equals(currentPassword)) {
                if (newPassword.equals(confirmPassword)) {
                    boolean isValidPassword = ValidationUtil.validatePassword(newPassword);
                    if (!isValidPassword) {
                        App.setFlashMessage(request, Messages.Credentials.PASSWORD_DOES_NOT_MEET_CRITERIA);
                    } else {
                        admin.setPassword(CipherUtil.encrypt(newPassword));
                        adminService.updateAdmin(admin);
                        App.setFlashMessage(request, Messages.UserPage.PASSWORD_SUCCESSFULLY_CHANGED);
                    }
                } else {
                    App.setFlashMessage(request, Messages.UserPage.PASSWORD_MISMATCH);
                }
            } else {
                App.setFlashMessage(request, Messages.UserPage.CURRENT_PASSWORD_MISMATCH);
            }
        }

        response.redirect("/admin/edit/");
        return new ModelAndView(null, "editAdmin.hbs");
    }
}
