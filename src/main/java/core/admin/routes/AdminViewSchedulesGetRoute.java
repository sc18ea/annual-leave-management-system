package core.admin.routes;

import REST.App;
import core.admin.Admin;
import core.admin.AdminSchedule;
import core.admin.AdminService;
import core.admin.AdminServiceImpl;
import core.user.User;
import core.user.UserService;
import core.user.UserServiceImpl;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.*;

import static util.Messages.HTML.FLASH_MESSAGE_KEY;

public class AdminViewSchedulesGetRoute implements TemplateViewRoute {

    private final UserService userService;

    private final AdminService adminService;

    public AdminViewSchedulesGetRoute() {
        userService = new UserServiceImpl();
        adminService = new AdminServiceImpl();
    }
    //get route to update the schedule for the admin after creating schedule
    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {

        Map<String, Object> model = new HashMap<>();

        Admin admin = request.session(true).attribute("user");
        long teamID = admin.getTeam().getId();
        List<User> users = userService.getUsersByTeamID(teamID);
        List<AdminSchedule> schedules = new ArrayList<>();
        if (users != null) {
            for (User user : users) {
                List<AdminSchedule> schedule = adminService.getSchedulesByUsername(user.getUsername());
                if (schedule != null) {
                    schedules.addAll(schedule);
                }
            }
        }

        schedules.sort(Comparator.comparing(AdminSchedule::getStartDate));
        model.put(FLASH_MESSAGE_KEY, App.getFlashMessage(request));
        model.put("schedules", schedules);
        return new ModelAndView(model, "viewDeleteSchedules.hbs");
    }
}
