package core.admin.routes;

import REST.App;
import core.admin.Admin;
import core.admin.AdminServiceImpl;
import core.team.Team;
import core.team.TeamServiceImpl;
import core.user.User;
import core.user.UserServiceImpl;
import org.apache.commons.lang3.StringUtils;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
import util.Messages;

import java.util.*;

import static util.Messages.AdminPage.ALREADY_WORKING;

public class AdminCreateSchedulesPostRoute implements TemplateViewRoute {
    private final TeamServiceImpl teamService;

    private final UserServiceImpl userService;

    private final AdminServiceImpl adminService;

    public AdminCreateSchedulesPostRoute() {
        teamService = new TeamServiceImpl();
        userService = new UserServiceImpl();
        adminService = new AdminServiceImpl();
    }
//post route for the scheduling functionality of the admin
    @Override
    public ModelAndView handle(Request request, Response response) {
        Map<String, List<String>> model = new HashMap<>();
        Admin admin = request.session(true).attribute("user");
        Team team = teamService.getTeamByAdminId(admin.getId());
        List<User> users = userService.getUsersByTeamID(team.getId());

        for (User user : users) {
            String username = user.getUsername();
            String from = request.queryParams(username);
            String to = request.queryParams(username + "2");

            if (StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)) {
                boolean acceptable = adminService.checkIfAcceptableSchedule(username, from, to);

                if (acceptable) {
                    adminService.addSchedule(username, from, to);
                } else {
                    ArrayList<String> objects = new ArrayList<>();
                    objects.add(ALREADY_WORKING);
                    model.put("nothing", objects);
                    ArrayList<String> result = new ArrayList<>();
                    users.forEach(u -> result.add(u.getUsername()));
                    model.put("listUsers", result);
                    App.setFlashMessage(request, ALREADY_WORKING);
                    response.redirect("/admin/schedules/create/");
                    return new ModelAndView(model, "createSchedule.hbs");
                }
            }
        }
        ArrayList<String> result = new ArrayList<>();
        users.forEach(u -> result.add(u.getUsername()));
        model.put("listUsers", result);

        App.setFlashMessage(request, Messages.AdminPage.SCHEDULE_UPDATE);
        response.redirect("/admin/schedules/create/");
        return new ModelAndView(model, "createSchedule.hbs");
    }
}
