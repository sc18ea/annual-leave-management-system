package core.admin.routes;

import liquibase.pro.packaged.M;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
//get route for the admin's account to download attached files
public class AdminFileGetRoute implements TemplateViewRoute {
    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String uri = request.uri();
        String filePath = uri.substring(uri.indexOf("-") + 1);
        File file = new File(filePath);
        response.raw().setContentType("application/octet-stream");
        response.raw().setHeader("Content-Disposition","attachment; filename="+file.getName());
        try {

            try(OutputStream outputStream = new FileOutputStream(filePath);
                BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file)))
            {
                byte[] buffer = new byte[1024];
                int len;
                while ((len = bufferedInputStream.read(buffer)) > 0) {
                    outputStream.write(buffer,0,len);
                }
                outputStream.flush();
            }
        } catch (Exception e) {
            //TODO some logs?
        }
        response.raw();
        return new ModelAndView(null, "admin.hbs");
    }
}
