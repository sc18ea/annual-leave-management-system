package core.admin;

import core.team.Team;
import core.user.User;
import core.vacation.Vacation;
import enumerate.Status;

import java.util.List;

public interface AdminService {
    void changeVacationStatus(String adminName, long requestId, Status newStatus);

    Admin getAdminByUsername(String username);

    Admin getAdminByTeamId(long teamId);

    boolean checkIfAdminExists(String username, String encryptedPassword);

    boolean checkIfAcceptableSchedule(String username, String from, String to);

    boolean isAuthorized(Admin admin, Team team);

    // generating report in json format
    String generateVacationReport(long daysAvailable, long daysConsumed, Admin admin);

    String tryToApprove(long requestId, String reason, Vacation vacation, Team team, User user, Admin admin);

    void addSchedule(String username, String from, String to);

    void removeSchedule(AdminSchedule schedule);

    AdminSchedule getScheduleByUsername(String username);

    List<AdminSchedule> getSchedulesByUsername(String username);

    AdminSchedule getScheduleById(long id);

    void updateAdmin(Admin admin);

}
