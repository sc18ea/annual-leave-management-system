# Annual leave management system
This is a leave management tool provided as an open-source. It is a Java web application where employees in an organisation can request leaves.
The application's goal is to provide a free platform for start-ups and low budgeted companies. Creating schedules by the admins is also implemented
as an additional feature.

## Prerequisites
Only Docker Engine is required in order to run the project. Other tools and frameworks are not required since all of the pre-requisites have been
containerized and uploaded to the Docker image.

## How to run it
Ensure Docker Engine is downloaded, set and running on your machine!

Following the below mentioned steps in order to run the application: 

1.Open the terminal/command prompt on the application's directory

2.Use the following commands to build the image and start the server

	1.docker build -t university .
	2.docker compose up -d

3.The server runs on localhost:8080 !
Open browser and enter: http://localhost:8080/

4.The application should be up and running successfully	

## Info
There are 4 teams with admins registered for the purpose of the demo. If you want to put/change data you should do it with sql queries directly to database.

| Teams 	  | Admins account | Password | Email address:    |
| ---------   | -------------- | -------- | ----------------- |
| Developers  | devadmin 	   | 1234	  | admintest@mail.bg |
| Accountants | accadmin 	   | 1234     | test@mail.bg	  |
| QA 		  | qaadmin 	   | 1234 	  | test2@mail.bg 	  |
| Managers 	  | mngadmin 	   | 1234 	  | test3@mail.bg 	  |


Real email account is created for the purpose of the demo. The credentials are provided below. It is active only for the admin of the Developers team to serve 
as example during demonstration. 
The other specified emails are provided as example, and are not active.

Email: admintest@mail.bg 
Password: 12345


## Tools and Frameworks used for the project
 - [Maven](https://maven.apache.org/) - Dependency Management
-  [Spark Framework](http://sparkjava.com/) - Web framework
-  [Hibernate](https://hibernate.org/) - ORM
-  [MySQL](https://www.mysql.com/) - Database
-  [Liquibase](https://www.liquibase.org/) - Source control for database;managing and initial setup

