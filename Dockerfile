FROM openjdk:11 

# Install maven
RUN apt-get update
RUN apt-get install -y maven

WORKDIR /diploma

# Prepare by downloading dependencies
ADD pom.xml /diploma/pom.xml
RUN ["mvn", "dependency:resolve"]
RUN ["mvn", "verify"]

# Adding source, compile and package into a fat jar
ADD src /diploma/src
RUN mkdir -p /diploma/files
RUN ["mvn", "verify"]

EXPOSE 8080
CMD ["java", "-jar", "target/web-rest-api-1.0-SNAPSHOT-jar-with-dependencies.jar"]